package ru.gbezyuk;

/**
 * Created by gbezyuk on 16.08.2014
 *
 * This is a simple calculator. User enters an arithmetic expression, the program gives back the result.
 *
 * Based on Operator Precedence Parsing algorithms (see http://en.wikipedia.org/wiki/Operator-precedence_parser)
 *
 */
public class Main {

    public static void main(String[] args) {
        Parser parser = new Parser();
//        parser.setDebugMode(true);
        try {
            System.out.println(parser.evaluate("11 + (exp(2.010635 + sin(PI/2)*3) + 50) / 2"));
        }
        catch (ParserException pe) {
            System.out.println("ERROR: " + pe.toString());
        }
    }
}
