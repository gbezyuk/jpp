package ru.gbezyuk;
import java.util.HashMap;

public class Parser {

    final int NONE = 0;
    final int DELIMITER = 1;
    final int VARIABLE = 2;
    final int FUNCTION = 3;
    final int NUMBER = 4;

    final int SYNTAXERROR = 0;
    final int UNBALPARENS = 1;
    final int NOEXP = 2;
    final int DIVBYZERO = 3;
    final int WRONGVARIABLE = 4;
    final int WRONGFUNCTION = 5;

    final String EOF = "\0";

    private String exp;     //  expression string
    private int expPtr;     //  current parsing point pointer
    private String token;   //  current token
    private int tokenType;    //  current token type

    private HashMap<String, Double> variables;

    public void setVariable(String variableName, Double variableValue) {
        variables.put(variableName, variableValue);
    }

    public Double getVariable(String variableName) throws ParserException {
        debugMessage("getVariable " + variableName);
        if (!variables.containsKey(variableName)) {
            handleErr(WRONGVARIABLE);
            return 0.0;
        }
        debugMessage(variableName + " = " + variables.get(variableName));
        return variables.get(variableName);
    }

    public Parser () {
        variables = new HashMap<String, Double>();
        setVariable("E", Math.E);
        setVariable("PI", Math.PI);
    }

    private boolean debugMode = false;

    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
    }

    private void debugMessage(String message) {
        if (debugMode) {
            System.out.println(message);
        }
    }

    private void debugState() {
        if (debugMode) {
            System.out.println(this);
        }
    }

    private String tokenTypeAsString(int tokenType) {
        switch (tokenType) {
            case NONE: return "NONE";
            case DELIMITER: return "DELIMITER";
            case VARIABLE: return "VARIABLE";
            case FUNCTION: return "FUNCTION";
            case NUMBER: return "NUMBER";
        }
        return "ERROR";
    }

    // string representation of a parser state for a debugMode purposes
    public String toString() {
        return "Exp = " + exp + "; expPtr = " + expPtr + "; Token = " + token
                + "; TokType = " + tokenTypeAsString(tokenType);
    }

    // moves expression pointer to the next token, updating token and tokenType
    private void moveToNextToken(){
        tokenType = NONE;
        token = "";

        // skip spaces
        while(expPtr < exp.length() && Character.isWhitespace(exp.charAt(expPtr)))
            ++ expPtr;

        // handle EOF
        if(expPtr == exp.length()){
            token = EOF;
            return;
        }

        // handle DELIMETER tokens
        if(isDelim(exp.charAt(expPtr))){
            token += exp.charAt(expPtr);
            expPtr++;
            tokenType = DELIMITER;
        }
        // handle VARIABLE tokens
        else if(Character.isLetter(exp.charAt(expPtr))){
            while(!isDelim(exp.charAt(expPtr))){
                token += exp.charAt(expPtr);
                expPtr++;
                if(expPtr >= exp.length())
                    break;
            }
            tokenType = expPtr < exp.length() && exp.charAt(expPtr) == '(' ? FUNCTION : VARIABLE;
        }
        // handle NUMBER tokens
        else if (Character.isDigit(exp.charAt(expPtr))){
            while(!isDelim(exp.charAt(expPtr))){
                token += exp.charAt(expPtr);
                expPtr++;
                if(expPtr >= exp.length())
                    break;
            }
            tokenType = NUMBER;
        }
        // anything else considered EOF; syntax error would be raised higher in the call stack
        else {
            token = EOF;
        }

        debugState();
    }

    // This function handles both operators and spaces; yet spaces are skipped where necessary in moveToNextToken()
    private boolean isDelim(char charAt) {
        return (" +-/*%^=()".indexOf(charAt)) != -1;
    }

    // Main parser entry point
    public double evaluate(String expressionString) throws ParserException{

        double result;

        exp = expressionString;
        expPtr = 0;
        moveToNextToken();

        if(token.equals(EOF))
            handleErr(NOEXP);

        // precedence parser entry point
        result = evaluatePrecedence2();

        if(!token.equals(EOF))
            handleErr(SYNTAXERROR);

        return result;
    }

    // precedence 2 handles binary operators of + and -
    private double evaluatePrecedence2() throws ParserException{
        debugMessage("evaluate 2: " + token);
        char op;
        double result;
        double partialResult;
        result = evaluatePrecedence3();
        while((op = token.charAt(0)) == '+' ||
                op == '-'){
            moveToNextToken();
            partialResult = evaluatePrecedence3();
            switch(op){
                case '-':
                    result -= partialResult;
                    break;
                case '+':
                    result += partialResult;
                    break;
            }
        }
        return result;
    }

    // precedence 3 handles binary operators of *, / and %
    private double evaluatePrecedence3() throws ParserException{
        debugMessage("evaluate 3: " + token);

        char op;
        double result;
        double partialResult;

        result = evaluatePrecedence4();
        while((op = token.charAt(0)) == '*' ||
                op == '/' | op == '%'){
            moveToNextToken();
            partialResult = evaluatePrecedence4();
            switch(op){
                case '*':
                    result *= partialResult;
                    break;
                case '/':
                    if(partialResult == 0.0)
                        handleErr(DIVBYZERO);
                    result /= partialResult;
                    break;
                case '%':
                    if(partialResult == 0.0)
                        handleErr(DIVBYZERO);
                    result %= partialResult;
                    break;
            }
        }
        return result;
    }

    // precedence 4 handles exponential binary operator of ^
    private double evaluatePrecedence4() throws ParserException{
        debugMessage("evaluate 4: " + token);

        double result;
        double partialResult;
        double ex;
        int t;
        result = evaluatePrecedence5();
        if(token.equals("^")){
            moveToNextToken();
            partialResult = evaluatePrecedence4();
            ex = result;
            if(partialResult == 0.0){
                result = 1.0;
            }else
                for(t = (int)partialResult - 1; t >  0; t--)
                    result *= ex;
        }
        return result;
    }

    // precedence 5 handles unary operators of + and - and also function calls
    private double evaluatePrecedence5() throws ParserException{
        debugMessage("evaluate 5: " + token);

        double result;

        String op = " ";
        boolean functionFound = false;

        if((tokenType == DELIMITER) && token.equals("+") || token.equals("-")){
            debugMessage("DELIMITER: " + token);
            op = token;
            moveToNextToken();
        }
        else if(tokenType == FUNCTION) {
            functionFound = true;
            debugMessage("FUNCTION: " + token);
            op = token;
            moveToNextToken();
        }
        result = evaluatePrecedence6();
        if(op.equals("-"))
            result =  -result;
        else if(functionFound) {
            result = evaluateFunction(op, result);
        }

        return result;
    }

    private double evaluateFunction(String functionName, double partialResult) throws ParserException {
        debugMessage("evaluateFunction: " + functionName + "(" + partialResult + ")");

        if (functionName.equals("sin")) {
            return Math.sin(partialResult);
        }
        else if (functionName.equals("cos")) {
            return Math.cos(partialResult);
        }
        else if (functionName.equals("tan")) {
            return Math.tan(partialResult);
        }
        else if (functionName.equals("exp")) {
            return Math.exp(partialResult);
        }
        handleErr(WRONGFUNCTION);
        return partialResult;
    }

    // precedence 6 handles parenthesis
    private double evaluatePrecedence6() throws ParserException{
        debugMessage("evaluate 6: " + token);

        double result;

        if(token.equals("(")){
            moveToNextToken();
            result = evaluatePrecedence2();
            if(!token.equals(")"))
                handleErr(UNBALPARENS);
            moveToNextToken();
        }
        else
            result = atom();
        return result;
    }

    // evaluating atoms, which could be numbers, variables/constants and function names
    private double atom()   throws ParserException{
        debugMessage("atom: " + token + ", " + tokenTypeAsString(tokenType));
        double result = 0.0;
        switch(tokenType) {
            case VARIABLE:
                result = getVariable(token);
                moveToNextToken();
                break;
            case NUMBER:
                try{
                    result = Double.parseDouble(token);
                }
                catch(NumberFormatException exc){
                    handleErr(SYNTAXERROR);
                }
                moveToNextToken();

                break;
            default:
                handleErr(SYNTAXERROR);
                break;
        }
        return result;
    }

    // translating inner error codes to human-readable format
    private void handleErr(int errorCode) throws ParserException{

        String[] errorDescriptions  = {
                "Syntax error",
                "Unbalanced Parentheses",
                "No Expression Present",
                "Division by zero",
                "Undefined variable or constant",
                "Undefined function",
        };
        throw new ParserException(errorDescriptions[errorCode]);
    }
}
